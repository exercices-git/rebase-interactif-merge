# Exercice Rebase Interactif Merge

L'objectif est d'utiliser la commande `rebase` de manière interactive pour réécrire les branches de l'historique

## Déroulé

* Cloner le projet avec la commande `git clone https://gitlab.com/exercices-git/rebase-interactif-merge.git`
* Utiliser la commande `git rebase -i 61f4adb` pour démarrer le rebase interactif (en temps normal on utiliserait `git rebase -i` puisque les commits n'auraient pas été poussés vers le dépôt public)

## Résultat

On veut que l'historique final ressemble à :

```
*   58a56c9 (HEAD -> master) Merge branch 'voiture'
|\  
| * 29d24ab fix: mv voiture.yaml in yaml folder
| * 5396e5c fix: poids dans voiture.yaml
| * 34d51cd fix: dimensions in voiture.yaml
| * 6e025d1 feat: voiture.yaml
|/  
*   687f6a5 Merge branch 'societe'
|\  
| * 055d6a2 fix: mv societe.yaml in yaml folder
| * 27f2b59 fix: societe.yaml missing :
| * 2607ed1 societe.yaml
|/  
*   473ef4b Merge branch 'contact'
|\  
| * 61cc271 fix: french keys in contact.yaml
| * 43c4a12 fix: birthdate in contact.yaml
| * e87961b feat: contact.yaml
|/  
* a20c01a chore: logs/.gitkeep
* 185f7ef chore: .gitignore
* 6112d6c feat: enoncé de l'exercice
* 61f4adb chore: commit initial
```
